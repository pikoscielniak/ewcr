var should = require('should'),
    mock = require('mockery'),
    mongoose = require('mongoose');
//var membershipRepository = {};

var porjId1 = new mongoose.Types.ObjectId;
var porjId2 = new mongoose.Types.ObjectId;


function Project(projectId) {
    var self = this;
    this._id = projectId;
    this.name = 'proj ' + projectId;
    this.toObject = function () {
        return self;
    }
};

var getWorkCardItem = function (day, hour, duration, projectId) {


    return {
        startTime: new Date(2013, 0, day, hour, 0, 0, 0),
        endTime: new Date(2013, 0, day, hour + duration, 0, 0, 0),
        isInProgress: false,
        project: new Project(projectId)
    };
};

var workCard = {
    month: 1,
    year: 2013,
    employee: 1,
    workCardItems: [
        getWorkCardItem(1, 8, 4, porjId1),
        getWorkCardItem(1, 12, 2, porjId2),
        getWorkCardItem(1, 14, 2, porjId2),
        getWorkCardItem(2, 8, 4, porjId2),
        getWorkCardItem(3, 8, 4, porjId1),
        getWorkCardItem(3, 12, 4, porjId2)
    ]
};


describe('workCardPreparer', function () {
    describe("prepareWorkCard", function () {
        beforeEach(function () {
            mock.enable();
            //mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerAllowable('../core/workCardStaff/workCardPreparer');
            mock.registerAllowable('../workCardStaff/totalHoursCalculator');
            mock.registerAllowable('underscore');
        });
        it("should return object with user property", function (done) {

            var preparer = require('../core/workCardStaff/workCardPreparer');
            var result = preparer.prepareWorkCard(31, workCard.workCardItems);

            result.length.should.be.equal(2);
            should.exist(result[0].project);
            should.exist(result[1].project);
            result[0].days[0].should.equal(4);
            result[1].days[0].should.equal(4);
            result[1].days[1].should.equal(4);
            result[0].days[2].should.equal(4);
            result[1].days[2].should.equal(4);
            done();
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
    describe("createReport", function () {
        beforeEach(function () {
            mock.enable();
            //mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerAllowable('../core/workCardStaff/workCardPreparer');
            mock.registerAllowable('../workCardStaff/totalHoursCalculator');
            mock.registerAllowable('underscore');
        });
        it("should return propert object", function (done) {

            var preparer = require('../core/workCardStaff/workCardPreparer');
            var result = preparer.createReport(31, workCard.workCardItems);

            result.length.should.be.equal(2);
            should.exist(result[0].project);
            should.exist(result[1].project);
            result[0].hours.should.equal(8);
            result[1].hours.should.equal(12);
            done();
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

