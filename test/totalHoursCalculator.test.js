var totalCalc = require('../core/workCardStaff/totalHoursCalculator'),
    should = require('should');

describe('totalHoursCalculator', function () {
    describe("calculateTotalHours", function () {
        it("should return 8 when is exactly 8 hours difference", function () {
            var startDate = new Date(2013, 3, 3, 6, 0, 0, 0);
            var endDate = new Date(2013, 3, 3, 14, 0, 0, 0);
            var result = totalCalc.calculateTotalHours(startDate, endDate);
            result.should.equal(8);
        });
        it("should return round down hour", function () {
            var startDate = new Date(2013, 3, 3, 6, 0, 0, 0);
            var endDate = new Date(2013, 3, 3, 15, 30, 0, 0);
            var result = totalCalc.calculateTotalHours(startDate, endDate);
            result.should.equal(9);
        });
        it("should return proper value when dates are different in days", function () {
            var startDate = new Date(2013, 3, 3, 22, 0, 0, 0);
            var endDate = new Date(2013, 3, 4, 6, 30, 0, 0);
            var result = totalCalc.calculateTotalHours(startDate, endDate);
            result.should.equal(8);
        });
    });
});