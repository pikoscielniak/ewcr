var should = require('should'),
    loginer = require('../services/loginer');

var request;
var response;

describe('loginer', function () {
    describe("authenticate", function () {
        beforeEach(function () {
            request = {
                session: {}
            };
            response = {};
            response.render = function () {
            };
        });
        it("should return render index page", function (done) {
            response.render = function (path,obj) {
                path.should.equal('index');
                obj.title.should.be.ok;
                obj.errorMsg.should.have.property;
                done();
            };
            loginer.authenticate(request, response, null);
        });
        it("should call render proper view when user is logged and request is not ajax", function (done) {
            request.session.user = {};
            request.session.user.isAuthenticated = true;
            request.session.user.isHr = true;
            response.render = function (path) {
                path.should.be.equal('indexHr');
                done();
            };
            loginer.authenticate(request, response);
        });
        it("should call next when user is logged and is ajax request", function (done) {
            request.session.user = {};
            request.session.user.isAuthenticated = true;
            request.session.user.isHr = true;
            request.xhr = true;
            var next = function () {
                done();
            };
            loginer.authenticate(request, response, next);
        });
        it("should allow access to /", function (done) {
            request.path = '/';
            var next = function () {
                done();
            };
            loginer.authenticate(request, response, next);
        });
        it("should allow access to /logout when user is logged in", function (done) {
            request.path = '/logout';
            request.session.user = {};
            request.session.user.isAuthenticated = true;
            var next = function () {
                done();
            };
            loginer.authenticate(request, response, next);
        });
    });

});