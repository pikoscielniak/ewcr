var viewDispatcher = require('../services/viewDispatcher'),
    should = require('should');

describe('viewDispatcher', function () {
    describe("dispatchView", function () {
        it("should return proper view for hr user", function (done) {
            var user = {
                isHr: true
            };
            var res = {
                render: function (path) {
                    path.should.be.equal('indexHr');
                    done();
                }
            };
            viewDispatcher.dispatchView(res, user);
        });
        it("should return proper view for employee user", function (done) {
            var res = {
                render: function (path) {
                    path.should.be.equal('indexEmployee');
                    done();
                }
            };
            viewDispatcher.dispatchView(res, false);
        });
    });
});