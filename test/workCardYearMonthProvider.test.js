var should = require('should'),
    mock = require('mockery');
var dateTimeProvider = {};
describe('workCardYearMonthProvider', function () {
    describe("getCurrent", function () {
        beforeEach(function () {
            mock.enable();
            dateTimeProvider.getNow = function () {
                return new Date();
            };
            mock.registerMock('./dateTimeProvider', dateTimeProvider);
            mock.registerAllowable('../core/workCardStaff/workCardYearMonthProvider');
        });
        it("should return proper year and month", function (done) {
            var date = new Date();
            var yearMonthProv = require('../core/workCardStaff/workCardYearMonthProvider');
            var obj = yearMonthProv.getCurrent();
            obj.year.should.be.equal(date.getFullYear());
            obj.month.should.be.equal(date.getMonth());
            done();
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

