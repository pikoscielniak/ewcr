var should = require('should'),
    mock = require('mockery'),
    appConig = require('../core/app/config'),
    testEnv = require('../test_expensive/testEnvironment');

var hrPosition = appConig.employeePositions.humanResourcesManager;
var hrKind = appConig.employeeKind.whiteCollarWorker;
var hrRoles = [appConig.systemRoles.employee, appConig.systemRoles.humanResources];

var empPosition = appConig.employeePositions.employee;
var empKind = appConig.employeeKind.blueCollarWorker;
var empRoles = [appConig.systemRoles.employee];

var request = {};
var response = {};

var employee = {};
employee.login = "1";
employee.password = 'p';
employee.symbol = "s";
employee.firstName = "fn";
employee.lastName = "ln";

var membershipRepository = {};
var employeeRepository = {};

describe('employees', function () {
    describe("add", function () {
        beforeEach(function () {
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerMock('../db/employeeRepository', employeeRepository);
            membershipRepository.createNew = function () {
            };
            request.body = {};
            response.json = function () {

            };
            mock.registerAllowable('../routes/employees');
            mock.registerAllowable('../services/rolesChecker');
            mock.registerAllowable('../core/app/config');
        });
        it("should insert hr employee", function (done) {
            employee.isHr = true;
            membershipRepository.createNew = function (member, callback) {
                member.login.should.be.equal(employee.login);
                member.password.should.be.equal(employee.password);
                member.employee.symbol.should.be.equal(employee.symbol);
                member.employee.firstName.should.be.equal(employee.firstName);
                member.employee.lastName.should.be.equal(employee.lastName);
                member.employee.position.should.eql(hrPosition);
                member.employee.kind.should.eql(hrKind);
                member.employee.roles.should.eql(hrRoles);
                done();
            };
            request.body = employee;
            var employees = require('../routes/employees');
            employees.add(request, response);
        });

        it("should insert employee", function (done) {
            employee.isHr = false;
            membershipRepository.createNew = function (member, callback) {
                member.login.should.be.equal(employee.login);
                member.password.should.be.equal(employee.password);
                member.employee.symbol.should.be.equal(employee.symbol);
                member.employee.firstName.should.be.equal(employee.firstName);
                member.employee.lastName.should.be.equal(employee.lastName);
                member.employee.position.should.eql(empPosition);
                member.employee.kind.should.eql(empKind);
                member.employee.roles.should.eql(empRoles);
                done();
            };
            request.body = employee;
            var employees = require('../routes/employees');
            employees.add(request, response);
        });

        it("should return status true when error doesn't occure", function (done) {
            employee.isHr = false;
            response.json = function (obj) {
                obj.status.should.be.true;
                done();
            };
            membershipRepository.createNew = function (member, callback) {
                callback(null, {_id: 1});
            };

            request.body.employee = employee;
            var employees = require('../routes/employees');
            employees.add(request, response);
        });

        it("should return status false when error does occure", function (done) {
            employee.isHr = false;
            response.json = function (obj) {
                obj.status.should.be.false;
                done();
            };
            membershipRepository.createNew = function (member, callback) {
                callback({}, null);
            };

            request.body.employee = employee;
            var employees = require('../routes/employees');
            employees.add(request, response);
        });

        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });

    describe("getAll", function () {
        beforeEach(function () {
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerMock('../db/employeeRepository', employeeRepository);

            employeeRepository.getAll = function () {
            };

            membershipRepository.createNew = function () {
            };

            request.body = {};
            response.json = function () {
            };
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../routes/employees');
        });

        it("should return all employees", function (done) {
            employeeRepository.getAll = function () {
                done();
            };
            var employees = require('../routes/employees');
            employees.getAll(request, response);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        })
    });

    describe("remove", function () {
        beforeEach(function () {
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);

            request.body = {};
            response.json = function () {
            };
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../routes/employees');
        });

        it("should remove employee", function (done) {
            membershipRepository.remove = function () {
                done();
            };
            var employees = require('../routes/employees');
            employees.remove(request, response);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        })
    });

    describe("get", function () {
        beforeEach(function () {
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);

            request.body = {};
            response.json = function () {
            };
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../routes/employees');
        });

        it("should return employee", function (done) {
            var emp = testEnv.getMembership('log', 'pas', 'sym');
            membershipRepository.getMembershipById = function (id, callback) {
                emp._id = 3;
                callback(null, emp);
            };
            response.json = function (obj) {
                obj.login.should.be.equal(emp.login);
                should.not.exist(obj.password);
                obj.symbol.should.be.equal(emp.employee.symbol);
                obj.firstName.should.be.equal(emp.employee.firstName);
                obj.lastName.should.be.equal(emp.employee.lastName);
                obj.isHr.should.be.false;
                done();
            };
            request.params = {};
            request.params.id = 3;
            var employees = require('../routes/employees');
            employees.get(request, response);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        })
    });

    describe("update", function () {
        beforeEach(function () {
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);

            membershipRepository.update = function () {

            };

            request.body = {};
            response.json = function () {
            };
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../routes/employees');
        });

        it("should update employee without password", function (done) {
            employee.isHr = true;
            membershipRepository.update = function (member, callback) {
                member.login.should.be.equal(employee.login);
                should.not.exist(member.password);
                member.employee.symbol.should.be.equal(employee.symbol);
                member.employee.firstName.should.be.equal(employee.firstName);
                member.employee.lastName.should.be.equal(employee.lastName);
                member.employee.position.should.eql(hrPosition);
                member.employee.kind.should.eql(hrKind);
                member.employee.roles.should.eql(hrRoles);
                done();
            };
            employee.password = undefined;
            request.body = employee;
            var employees = require('../routes/employees');
            employees.update(request, response);
        });
        it("should update employee with password", function (done) {
            employee.isHr = true;
            membershipRepository.update = function (member, callback) {
                member.login.should.be.equal(employee.login);
                member.password.should.be.equal(employee.password);
                member.employee.symbol.should.be.equal(employee.symbol);
                member.employee.firstName.should.be.equal(employee.firstName);
                member.employee.lastName.should.be.equal(employee.lastName);
                member.employee.position.should.eql(hrPosition);
                member.employee.kind.should.eql(hrKind);
                member.employee.roles.should.eql(hrRoles);
                done();
            };
            employee.password = 'p';
            request.body = employee;
            var employees = require('../routes/employees');
            employees.update(request, response);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        })
    });
});

