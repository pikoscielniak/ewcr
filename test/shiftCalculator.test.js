var shiftCalculator = require('../core/workCardStaff/shiftCalculator'),
    should = require('should');
var firstStart = shiftCalculator.getFirstShiftStart();
var sndStart = shiftCalculator.getSecondShiftStart();
var thirdStart = shiftCalculator.getThirdShiftStart();

describe('shiftCalculator', function () {
    describe("calculateShift", function () {
        it("should return one when hour is " + firstStart, function () {
            var date = new Date(2013, 4, 2, firstStart, 0, 0, 0);
            var result = shiftCalculator.calculateShift(date);
            result.should.equal(1);
        });
        it("should return one when hour is between " + firstStart
            + " and " + sndStart, function () {
            var date1 = new Date(2013, 4, 2, firstStart + 1, 0, 0, 0);
            var result1 = shiftCalculator.calculateShift(date1);
            result1.should.equal(1);

            var date2 = new Date(2013, 4, 2, sndStart - 1, 0, 0, 0);
            var result2 = shiftCalculator.calculateShift(date2);
            result2.should.equal(1);
        });
        it("should return two when hour is " + sndStart, function () {
            var date = new Date(2013, 4, 2, sndStart, 0, 0, 0);
            var result = shiftCalculator.calculateShift(date);
            result.should.equal(2);
        });
        it("should return two when hour is between " + sndStart + " and "
            + thirdStart, function () {
            var date1 = new Date(2013, 4, 2, sndStart + 1, 0, 0, 0);
            var result1 = shiftCalculator.calculateShift(date1);
            result1.should.equal(2);

            var date2 = new Date(2013, 4, 2, thirdStart - 1, 0, 0, 0);
            var result2 = shiftCalculator.calculateShift(date2);
            result2.should.equal(2);
        });
        it("should return three when hour is " + thirdStart, function () {
            var date = new Date(2013, 4, 2, thirdStart, 0, 0, 0);
            var result = shiftCalculator.calculateShift(date);
            result.should.equal(3);
        });
        it("should return two when hour is between " + thirdStart + " and "
            + firstStart, function () {
            var date1 = new Date(2013, 4, 2, thirdStart + 1, 0, 0, 0);
            var result1 = shiftCalculator.calculateShift(date1);
            result1.should.equal(3);

            var date2 = new Date(2013, 4, 2, firstStart - 1, 0, 0, 0);
            var result2 = shiftCalculator.calculateShift(date2);
            result2.should.equal(3);
        });
    });
});