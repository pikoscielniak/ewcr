var hasher = require('../core/loggingStaff/hasher'),
    should = require('should');

var hashToCompare= '$2a$10$w3xgZ4ns46IuJMhW39C6f.N1VYfqDywuLMnkLpq5FqqP.6blr85MS';

describe('hasher', function () {
    describe("hash", function () {
        it("should hash", function (done) {
            var password = "1234";
            hasher.hash(password, function (err, hash) {
                should.not.exist(err);
                should.exist(hash)
                hash.should.be.a('string');
                hash.should.not.be.empty;
                hash.should.have.length(60);
                done();
            });
        });
    });
    describe("compare", function () {
        it("should comper password correctly", function (done) {
            hasher.compare("1234",hashToCompare, function (err, isMatch) {
                should.not.exist(err);
                isMatch.should.be.ok;
                done();
            });
        });
    });
});