var should = require('should'),
    mock = require('mockery');


describe('workCards', function () {
    var dateTimeProvider = {};
    describe("getCurrentTask", function () {
        var req = {};
        var res = {};
        var workCardRepository = {};
        var workCardProvider = {
            getNow: function () {
                return new Date();
            }
        };
        beforeEach(function () {
            req.session = {

            };
            mock.enable();
            mock.deregisterAll();
            mock.registerMock('../db/workCardRepository', workCardRepository);
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../routes/workCards');
            mock.registerMock('./dateTimeProvider', workCardProvider);
            mock.registerAllowable('../core/workCardStaff/workCardYearMonthProvider');
            mock.registerAllowable('../core/workCardStaff/dateTimeProvider');
            mock.registerAllowable('../core/workCardStaff/shiftsPreparer');
            mock.registerAllowable('../workCardStaff/shiftCalculator');
            mock.registerAllowable('../core/workCardStaff/workCardPreparer');
            mock.registerAllowable('../workCardStaff/totalHoursCalculator');
            mock.registerAllowable('underscore');
        });
        it("should return current task", function (done) {
            workCardRepository.getInProgress = function () {
                done();
            };
            req.session.user = {
                id: 1
            };
            var empWc = require('../routes/workCards');
            empWc.getCurrentTask(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

