var should = require('should'),
    mock = require('mockery'),
    config = require('../core/app/config');

var projectRepository = {};
var res = {};
var req = {};

var startDateStr = "2013-03-25";
var endDateStr = "2013-08-14";
var startDate = new Date(2013, 2, 25);
var endDate = new Date(2013, 7, 14);

var project = {
    number: 2,
    pwg: "PWG",
    name: 'Project test',
    description: 'description',
    startDate: startDateStr,
    endDate: endDateStr,
    status: config.projectStatuses.opened
};

describe('projects', function () {
    describe("add", function () {
        beforeEach(function () {

            mock.enable();
            mock.registerMock('../db/projectRepository', projectRepository);
            mock.registerAllowable('../routes/projects');
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../core/workCardStaff/dateTimeProvider');
            projectRepository.create = function () {

            };
            req.body = {};
        });
        it("should insert project", function (done) {
            projectRepository.create = function (proj) {
                proj.number.should.equal(2);
                proj.pwg.should.equal("PWG");
                proj.name.should.equal('Project test');
                proj.description.should.equal('description');
                proj.startDate.getTime().should.equal(startDate.getTime());
                proj.endDate.getTime().should.equal(endDate.getTime());
                proj.status.should.equal(config.projectStatuses.opened);
                done();
            };
            req.body = project;
            var projects = require('../routes/projects');
            projects.add(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });

    describe("getAll", function () {
        beforeEach(function () {

            mock.enable();
            mock.registerMock('../db/projectRepository', projectRepository);
            mock.registerAllowable('../routes/projects');
            mock.registerAllowable('../core/app/config');
            projectRepository.create = function () {

            };
            req.body = {};
        });
        it("should return projects", function (done) {
            projectRepository.getAll = function () {
                done();
            };
            req.body = project;
            var projects = require('../routes/projects');
            projects.getAll(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });

    describe("get", function () {
        beforeEach(function () {

            mock.enable();
            mock.registerMock('../db/projectRepository', projectRepository);
            mock.registerAllowable('../routes/projects');
            mock.registerAllowable('../core/app/config');
            projectRepository.create = function () {

            };
            req.body = {};
        });
        it("should return project", function (done) {
            projectRepository.getById = function () {
                done();
            };
            req.params = {};
            req.params.id
            var projects = require('../routes/projects');
            projects.get(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });

    describe("update", function () {
        beforeEach(function () {

            mock.enable();
            mock.registerMock('../db/projectRepository', projectRepository);
            mock.registerAllowable('../routes/projects');
            mock.registerAllowable('../core/app/config');
            projectRepository.create = function () {

            };
            projectRepository.getById = function () {

            };
            req.body = {};
        });
        it("should update project", function (done) {
            projectRepository.update = function () {
                done();
            };
            req.body = project;
            var projects = require('../routes/projects');
            projects.update(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });

    describe("remove", function () {
        beforeEach(function () {

            mock.enable();
            mock.registerMock('../db/projectRepository', projectRepository);
            mock.registerAllowable('../routes/projects');
            mock.registerAllowable('../core/app/config');
            projectRepository.create = function () {

            };
            projectRepository.getById = function () {

            };
            projectRepository.remove = function () {

            };
            req.body = {};
        });
        it("should remove project", function (done) {
            projectRepository.remove = function () {
                done();
            };
            req.body.id = 33;
            var projects = require('../routes/projects');
            projects.remove(req, res);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

