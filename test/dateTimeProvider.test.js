var should = require('should'),
    mock = require('mockery');
//var membershipRepository = {};
describe('dateTimeProvider', function () {
    describe("getDaysForMonth", function () {
        beforeEach(function () {
            mock.enable();
            //mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerAllowable('moment');
            mock.registerAllowable('../core/workCardStaff/dateTimeProvider');
            mock.registerAllowable('../app/config');
        });
        it("should return number of days for month", function (done) {

            var dateTime = require('../core/workCardStaff/dateTimeProvider');
            var year = 2013;
            var month = 2;
            var num = dateTime.getDaysForMonth(year, month);
            num.should.be.equal(31);
            done();
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

