var should = require('should'),
    mock = require('mockery');


var membershipRepository = {};
var dbInit = {};

describe('appInitiator', function () {
    describe('initApp', function () {
        beforeEach(function () {
            membershipRepository.createNew = function () {
            };
            membershipRepository.remove = function () {
            };
            membershipRepository.getMembership = function () {
            };
            dbInit.connectToDb = function () {
            };
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerMock('../db/dbInit', dbInit);
            mock.registerAllowable('../services/appInitiator');
            mock.registerAllowable('../core/app/config');
        });
        it("should connect to database", function (done) {
            dbInit.connectToDb = function () {
                done();
            };
            var appInitiator = require('../services/appInitiator');
            appInitiator.initApp();
        });
        it("should check if default user exists in database", function (done) {
            membershipRepository.getMembership = function () {
                done();
            };
            var appInitiator = require('../services/appInitiator');
            appInitiator.initApp();
        });
        it("should create user when doesn't exist", function (done) {
            membershipRepository.createNew = function () {
                done();
            };
            membershipRepository.getMembership = function (user, password, callback) {
                callback(null, null);
            };
            var appInitiator = require('../services/appInitiator');
            appInitiator.initApp();
        });

        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});
