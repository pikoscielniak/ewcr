var rolesChecker = require('../services/rolesChecker.js'),
    appConfig = require('../core/app/config'),
    should = require('should');

describe('rolesChecker', function () {
    describe("isUserInRole", function () {
        it("should return true when user is in role", function () {
            var role = appConfig.systemRoles.humanResources;
            var roles = [role];
            var result = rolesChecker.isUserInRole(role, roles);
            result.should.be.ok;
        });
        it("should return false when user is not in role", function () {
            var role = appConfig.systemRoles.employee;
            var roles = [ appConfig.systemRoles.humanResources];
            var result = rolesChecker.isUserInRole(role, roles);
            result.should.be.false
        });
    });
});