var should = require('should'),
    mock = require('mockery'),
    appConfig = require('../core/app/config');

var request = {};
request.session = {};
var response = {};
var membershipRepository = {};
var member = {
    _id: '1',
    login: 'login',
    employee: {
        roles: [appConfig.systemRoles.employee],
        firstName: 'jan',
        lastName: 'kow'
    }
};

describe('index', function () {
    describe("processLogin", function () {
        beforeEach(function () {
            request.session.user = null;
            request.body = {};
            request.body.login = "log";
            request.body.password = "pas";
            request.body.loginKind = "user"; //'hr';
            response.render = function () {

            };
            membershipRepository.createNew = function () {
            };
            mock.enable();
            mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerAllowable('../routes/index');
            mock.registerAllowable('../core/app/config');
            mock.registerAllowable('../services/rolesChecker');
            mock.registerAllowable('../services/viewDispatcher');
        });
        it('should return index when error occurs and setup proper message', function (done) {
            membershipRepository.getMembership = function (user, pass, callback) {
                callback({}, null);
            };
            response.render = function (path, obj) {
                path.should.equal('index');
                obj.errorMsg.should.be.equal('Błąd podczas logowania');
                done();
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        it("should render index with error message when user doesn't exist", function (done) {

            membershipRepository.getMembership = function (user, pass, callback) {
                callback(null, null);
            };
            response.render = function (path, obj) {
                path.should.equal('index');
                obj.errorMsg.should.be.equal('Nieprawidłowy login lub hasło');
                done();
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        it("should return login error when user has not access to desired view", function (done) {
            request.body.loginKind = 'hr';
            response.render = function (path, obj) {
                path.should.equal('index');
                obj.errorMsg.should.be.equal('Nieprawidłowy login lub hasło');
                done();
            };
            membershipRepository.getMembership = function (user, pass, callback) {
                callback(null, member);
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        it("should setup user in session when authentication succeed and redriect to /", function (done) {
            response.redirect = function (path) {
                path.should.be.equal('/');
                should.exist(request.session.user);
                request.session.user.isAuthenticated.should.be.true;
                request.session.user.isHr.should.be.false;
                done();
            };
            membershipRepository.getMembership = function (user, pass, callback) {
                callback(null, member);
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        it("should setup user as HR", function (done) {
            member.employee.roles = [appConfig.systemRoles.employee, appConfig.systemRoles.humanResources];
            request.body.loginKind = 'hr';
            response.redirect = function () {
                should.exist(request.session.user);
                request.session.user.isHr.should.be.true;
                done();
            };
            membershipRepository.getMembership = function (user, pass, callback) {
                callback(null, member);
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        it("should setup user as HR only when he/she wants it", function (done) {
            member.employee.roles = [appConfig.systemRoles.employee, appConfig.systemRoles.humanResources];
            request.body.loginKind = 'user';
            response.redirect = function () {
                should.exist(request.session.user);
                request.session.user.isHr.should.be.false;
                done();
            };
            membershipRepository.getMembership = function (user, pass, callback) {
                callback(null, member);
            };
            var index = require('../routes/index');
            index.processLogin(request, response);
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
    describe("processLogin", function () {
        it("should logout user and redirect to /", function (done) {
            request.session.user = {};
            response.redirect = function (path) {
                path.should.be.equal('/');
                done();
            };
            var index = require('../routes/index');
            index.processLogout(request,response);
        });
    });
});