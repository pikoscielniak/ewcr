var should = require('should'),
    mock = require('mockery');
//var membershipRepository = {};

var getWorkCardItem = function (day, hour, duration, projectId) {
    return {
        startTime: new Date(2013, 0, day, hour, 0, 0, 0),
        endTime: new Date(2013, 0, day, hour + duration, 0, 0, 0),
        isInProgress: false,
        project: projectId
    };
};

var workCardItems = [
    getWorkCardItem(1, 8, 4, 1),
    getWorkCardItem(1, 12, 4, 1),
    getWorkCardItem(2, 15, 8, 1),
    getWorkCardItem(5, 8, 8, 1)];
describe('shiftsPreparer', function () {
    describe("prepareShifts", function () {
        beforeEach(function () {
            mock.enable();
//            mock.registerMock('../db/membershipRepository', membershipRepository);
            mock.registerAllowable('../core/workCardStaff/shiftsPreparer');
            mock.registerAllowable('../workCardStaff/shiftCalculator');
            mock.registerAllowable('underscore');
        });
        it("should return proper shift in given day", function (done) {

            var shiftsPre = require('../core/workCardStaff/shiftsPreparer');
            var array = shiftsPre.prepareShifts(31, workCardItems);
            array.length.should.be.equal(31);
            array[0].should.be.equal(1);
            array[1].should.be.equal(2);
            array[4].should.be.equal(1);
            done();
        });
        afterEach(function () {
            mock.deregisterAll();
            mock.disable();
        });
    });
});

