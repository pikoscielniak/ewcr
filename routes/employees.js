var membershipRepository = require('../db/membershipRepository'),
    employeeRepository = require('../db/employeeRepository'),
    config = require('../core/app/config'),
    rolesChecker = require('../services/rolesChecker');

var getPosition = function (isHr) {
    if (isHr) {
        return  config.employeePositions.humanResourcesManager;
    }
    return config.employeePositions.employee;
};

var getKind = function (isHr) {
    if (isHr) {
        return config.employeeKind.whiteCollarWorker;
    }
    return config.employeeKind.blueCollarWorker;
};

var getRoles = function (isHr) {
    if (isHr) {
        return [config.systemRoles.employee, config.systemRoles.humanResources];
    }
    return [config.systemRoles.employee];
};

var getNewEmployee = function (member) {
    return {
        symbol: member.symbol,
        firstName: member.firstName,
        lastName: member.lastName,
        position: getPosition(member.isHr),
        kind: getKind(member.isHr),
        roles: getRoles(member.isHr)
    };
};

var getNewMembership = function (member) {
    return {
        login: member.login,
        password: member.password,
        employee: getNewEmployee(member)
    }
};

var add = function (req, res) {
    var member = getNewMembership(req.body);
    membershipRepository.createNew(member, function (err, member) {
        if (err) {
            res.json({status: false, errorMsg: err});
        } else {
            res.json({status: true, id: member._id});
        }
    });
};

var getAll = function (req, res) {
    employeeRepository.getAll(function (err, data) {
        if (err) {
            res.json(null);
        } else {
            res.json(data);
        }
    });
};

var remove = function (req, res) {
    membershipRepository.remove(req.body.id, function (err) {
        if (err) {
            res.json(null);
        } else {
            res.json({status: true});
        }
    });
};

var getIsHr = function (employee) {
    return rolesChecker.isUserInRole(config.systemRoles.humanResources, employee.roles);
};

var createEmployeeVm = function (employeeDb) {
    var member = {};
    member.id = employeeDb._id;
    member.login = employeeDb.login;
    member.symbol = employeeDb.employee.symbol;
    member.firstName = employeeDb.employee.firstName;
    member.lastName = employeeDb.employee.lastName;
    member.isHr = getIsHr(employeeDb.employee);
    return member;
};

var get = function (req, res) {
    membershipRepository.getMembershipById(req.params.id, function (err, membership) {
        if (err) {
            res.json(null);
        } else {
            res.json(createEmployeeVm(membership));
        }
    });
};

var update = function (req, res) {
    var member = getNewMembership(req.body);
    member.id = req.body.id;
    membershipRepository.update(member, function (err, membership) {
        if (err) {
            res.json(null);
        } else {
            res.json({status: true});
        }
    });
};

exports.add = add;
exports.getAll = getAll;
exports.remove = remove;
exports.get = get;
exports.update = update;