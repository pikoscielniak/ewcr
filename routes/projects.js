var projectRepository = require('../db/projectRepository'),
    config = require('../core/app/config'),
    dateTime = require('../core/workCardStaff/dateTimeProvider');


var createProject = function (data) {
    return {
        number: data.number,
        pwg: data.pwg,
        name: data.name,
        description: data.description,
        startDate: dateTime.parseDate(data.startDate),
        endDate: dateTime.parseDate(data.endDate),
        status: data.status
    };
};

var add = function (req, res) {
    var project = createProject(req.body);
    projectRepository.create(project, function (err, proj) {
        if (err) {
            res.json({status: false, message: err});
        } else {
            res.json({status: true, id: proj._id});
        }
    });
};

var getAll = function (req, res) {
    projectRepository.getAll(function (err, data) {
        if (err) {
            res.json(null);
        } else {
            res.json(data);
        }
    });
};

var convertDatesInProject = function (project) {
    project.startDate = dateTime.convertDate(project.startDate);
    project.endDate = dateTime.convertDate(project.endDate);
};
var get = function (req, res) {
    projectRepository.getById(req.params.id, function (err, project) {
        if (err) {
            res.json(null);
        } else {
            project = project.toObject();
            convertDatesInProject(project);
            res.json(project);
        }
    });
};

var update = function (req, res) {
    var project = createProject(req.body);
    project._id = req.body._id;
    projectRepository.update(project, function (err) {
        if (err) {
            res.json({status: false, message: err});
        } else {
            res.json({status: true});
        }
    });
};

var remove = function (req, res) {
    projectRepository.remove(req.body.id, function (err) {
        if (err) {
            res.json(null);
        } else {
            res.json({status: true});
        }
    });
};

exports.add = add;
exports.getAll = getAll;
exports.remove = remove;
exports.get = get;
exports.update = update;