var appConfig = require('../core/app/config'),
    rolesChecker = require('../services/rolesChecker'),
    viewDispatcher = require('../services/viewDispatcher');

var title = viewDispatcher.title;

var createResult = function (msg) {
    return {
        title: title,
        errorMsg: msg || ''
    };
};

exports.index = function (req, res) {
    res.render('index', createResult());
};

var membershipRepository = require('../db/membershipRepository');

var processError = function (res) {
    res.render('index', createResult('Błąd podczas logowania'));
};

var processLoginFailure = function (res) {
    res.render('index', createResult('Nieprawidłowy login lub hasło'));
};

var userWantsBeAsHr = function (userKind) {
    return userKind == 'hr';
};
var setupSession = function (req, member, userKind) {
    var isHr = rolesChecker.isUserInRole(appConfig.systemRoles.humanResources,
        member.employee.roles) && userWantsBeAsHr(userKind);
    req.session.user = {
        _id: member._id,
        login: member.login,
        firstName: member.employee.firstName,
        lastName: member.employee.lastName,
        symbol: member.employee.symbol,
        kind: member.employee.kind,
        isHr: isHr,
        isAuthenticated: true
    };
    return isHr;
};
var getDesiredRoleToSeeView = function (loginKind) {
    if (loginKind == 'hr') {
        return appConfig.systemRoles.humanResources;
    }
    return appConfig.systemRoles.employee;
};

var hasUserAccessForDesiredView = function (loginKind, roles) {
    var role = getDesiredRoleToSeeView(loginKind);
    return rolesChecker.isUserInRole(role, roles);
};

var processLogout = function (req, res) {
    req.session.user = null;
    res.redirect('/');
};


exports.processLogin = function (req, res) {
    var login = req.body.login;
    var password = req.body.password;
    var loginKind = req.body.loginKind;
    membershipRepository.getMembership(login, password, function (err, member) {
        if (err) {
            processError(res);
        } else if (!member) {
            processLoginFailure(res);
        } else {
            if (hasUserAccessForDesiredView(loginKind, member.employee.roles)) {
                var isHr = setupSession(req, member, loginKind);
                res.redirect('/');
            } else {
                processLoginFailure(res);
            }
        }
    });
};
exports.processLogout = processLogout;
