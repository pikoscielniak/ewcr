var workCardRepository = require('../db/workCardRepository'),
    dateTime = require('../core/workCardStaff/dateTimeProvider'),
    _ = require('underscore'),
    yearAndMonthProvider = require('../core/workCardStaff/workCardYearMonthProvider'),
    shitsPreparer = require('../core/workCardStaff/shiftsPreparer'),
    workCardPreparer = require('../core/workCardStaff/workCardPreparer');

var getCurrentTask = function (req, res) {
    var monthAndYear = yearAndMonthProvider.getCurrent();
    var employeeId = req.session.user._id;
    workCardRepository.getInProgress(employeeId, monthAndYear.year, monthAndYear.month, function (err, workCardItem) {
        if (err) {
            res.json({});
        } else {
            res.json(workCardItem);
        }
    });
};

var getNewWorkCardItem = function (projectId) {
    return {
        isInProgress: true,
        startTime: dateTime.getNow(),
        project: projectId
    };
};

var getCurrentItem = function (workCard) {
    return _.find(workCard.workCardItems, function (wci) {
        return wci.isInProgress == true;
    });
};

var createWorkCardItemDto = function (workCard) {
    var currentItem = getCurrentItem(workCard);
    currentItem = currentItem.toObject();
    currentItem.workCardId = workCard._id;
    return currentItem;
};
var startTracking = function (req, res) {
    var projectId = req.body.projectId;
    var employeeId = req.session.user._id;
    var yearAndMonth = yearAndMonthProvider.getCurrent();
    var workCardItem = getNewWorkCardItem(projectId);

    workCardRepository.addWorkCardItem(employeeId, yearAndMonth.year,
        yearAndMonth.month, workCardItem, function (err, workCard) {
            if (err) {
                res.json({status: false});
            } else {
                res.json({status: true, data: createWorkCardItemDto(workCard)})
            }
        });
};
var createWorkCardItem = function (body) {
    return {
        isInProgress: body.isInProgress,
        startTime: body.startTime,
        endTime: body.endTime,
        project: body.projectId,
        _id: body._id
    };
};

var stopTracking = function (req, res) {
    var workCardId = req.body.workCardId;
    req.body.isInProgress = false;
    req.body.endTime = dateTime.getNow();
    var workCardItem = createWorkCardItem(req.body);
    workCardRepository.updateWorkCardItem(workCardId, workCardItem, function (err) {
        if (err) {
            res.json({status: false, message: err});
        } else {
            res.json({status: true});
        }
    });
};

var prepareWorkCardData = function (year, month, employeeId, callback) {
    var result = {};
    workCardRepository.getWorkCard(employeeId, year, month, function (err, workCard) {
        if (err) {
            return callback(err);
        }
        if (workCard) {
            var daysInMonth = dateTime.getDaysForMonth(year, month);
            result.daysInMonth = daysInMonth;
            result.user = getWorkCardOwnerInfo(workCard.employee);
            result.shifts = shitsPreparer.prepareShifts(daysInMonth, workCard.workCardItems);
            result.workCardItems = workCardPreparer.prepareWorkCard(daysInMonth, workCard.workCardItems);
            return callback(null, result);
        }
        return callback(null, null);
    });
};
var getWorkCardOwnerInfo = function (user) {
    return {
        _id: user._id,
        symbol: user.symbol,
        lastName: user.lastName,
        firstName: user.firstName,
        kind: user.kind
    };
};
var getWorkCard = function (req, res) {
    var year = req.params.year;
    var month = req.params.month;
    var employeeId = req.session.user._id;
    prepareWorkCardData(year, month, employeeId, function (err, data) {
        if (err) {
            return res.json(null);
        }
        return res.json(data);
    });
};

var convertDates = function (items) {
    if (!items) {
        return;
    }
    _.each(items, function (item) {
        item.startTime = dateTime.convertDateTime(item.startTime);
        item.endTime = dateTime.convertDateTime(item.endTime);
    });
};

var sortWorkCardItems = function (workCard) {
    if (workCard.workCardItems) {
        workCard.workCardItems = _.sortBy(workCard.workCardItems, function (item) {
            return item.startTime;
        });
    }
};
var getTotalHours = function (report) {
    return _.reduce(report, function (memo, item) {
        return memo + item.hours;
    },0);
};
var getWorkCardForEmployee = function (req, res) {
    var employeeId = req.params.employeeId;
    var year = req.params.year;
    var month = req.params.month;

    workCardRepository.getWorkCard(employeeId, year, month, function (err, workCard) {
        if (err) {
            return res.json(null);
        }
        if (workCard) {
            var daysInMonth = dateTime.getDaysForMonth(year, month);
            var raport = workCardPreparer.createReport(daysInMonth, workCard.workCardItems);
            workCard = workCard.toObject();
            convertDates(workCard.workCardItems);
            sortWorkCardItems(workCard);
            workCard.raport = raport;
            workCard.totalHours = getTotalHours(raport);
            return res.json(workCard);
        }
        return res.json(null);
    });
};
var updateWorkCard = function (req, res) {
    var workCardItem = req.body.workCardItem;
    var workCardId = req.body.workCardId;
    workCardItem.project = workCardItem.project._id;
    workCardItem.startTime = dateTime.parseDateTime(workCardItem.startTime);
    workCardItem.endTime = dateTime.parseDateTime(workCardItem.endTime);

    workCardRepository.updateWorkCardItem(workCardId, workCardItem, function (err) {
        if (err) {
            return res.json({status: false, message: err});
        }
        return res.json({status: true});
    });
};

var removeWorkCardItem = function (req, res) {
    var workCardId = req.body.workCardId;
    var workCardItemId = req.body.workCardItemId;

    workCardRepository.removeWorkCardItem(workCardId, workCardItemId, function (err) {
        if (err) {
            return res.json({status: false});
        }
        return res.json({status: true});
    });
};

exports.updateWorkCard = updateWorkCard;
exports.getCurrentTask = getCurrentTask;
exports.startTracking = startTracking;
exports.stopTracking = stopTracking;
exports.getWorkCard = getWorkCard;
exports.getWorkCardForEmployee = getWorkCardForEmployee;
exports.updateWorkCard = updateWorkCard;
exports.removeWorkCardItem = removeWorkCardItem;