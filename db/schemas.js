var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var membershipSchema = new schema({
    login: {type: String, unique: true},
    password: {type: String, unique: true},
    employee: {type: schema.Types.ObjectId, ref: 'Employee'}
});

var employeeSchema = new schema({
    symbol: {type: String, unique: true},
    firstName: {type: String},
    lastName: {type: String},
    position: {type: String},
    kind: {type: String},
    roles: {type: [String]}
});

var projectSchema = new schema({
    number: {type: String, unique: true},
    pwg: {type: String},
    name: {type: String},
    description: {type: String},
    startDate: {type: Date},
    endDate: {type: Date},
    status: {type: String}
});

var workCardItemSchema = new schema({
    isInProgress: {type: Boolean},
    startTime: {type: Date},
    endTime: {type: Date},
    project: {type: schema.Types.ObjectId, ref: 'Project'}
});

var workCardSchema = new schema({
    month: {type: Number},
    year: {type: Number},
    employee: {type: schema.Types.ObjectId, ref: 'Employee'},
    workCardItems: {type: [workCardItemSchema]}
});


exports.employeeSchema = employeeSchema;
exports.projectSchema = projectSchema;
exports.workCardItemSchema = workCardItemSchema;
exports.workCardSchema = workCardSchema;
exports.membershipSchema = membershipSchema;