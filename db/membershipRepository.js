var models = require('./models'),
    hasher = require('../core/loggingStaff/hasher');

var Membership = models.Membership;
var Employee = models.Employee;

var createNewEmployee = function (employee) {
    return new Employee({
        symbol: employee.symbol,
        firstName: employee.firstName,
        lastName: employee.lastName,
        position: employee.position,
        kind: employee.kind,
        roles: employee.roles
    });
};

var createNewMembership = function (membership, employee) {
    return new Membership({
        login: membership.login,
        password: membership.password,
        employee: employee
    });
};
var createNew = function (membership, callback) {
    var employee = createNewEmployee(membership.employee);
    employee.save(function (err, employee) {
        if (err) {
            callback(err);
        } else {
            hasher.hash(membership.password, function (err, hash) {
                if (err) {
                    callback(err);
                } else {
                    membership.password = hash;
                    var newMembership = createNewMembership(membership, employee);
                    newMembership._id = employee._id;
                    newMembership.passoword = hash;
                    newMembership.save(callback);
                }
            });
        }
    });
};

var remove = function (membershipId, callback) {
    Employee.remove({ _id: membershipId }, function (err) {
        if (err) {
            callback(err);
        } else {
            Membership.remove({ _id: membershipId }, callback);
        }
    });
};

var getMembership = function (login, password, callback) {

    Membership.findOne({ login: login})
        .populate('employee')
        .exec(function (err, member) {
            if (err) {
                callback(err);
            } else {
                if (member) {
                    hasher.compare(password, member.password, function (err, isMath) {
                        if (err) {
                            callback(err);
                        } else {
                            if (isMath) {
                                member.password = "";
                                callback(null, member);
                            } else {
                                callback(null, null);
                            }
                        }
                    });
                } else {
                    callback(null, null);
                }
            }
        });
};

var getMembershipById = function (id, callback) {
    Membership.findOne({ _id: id})
        .populate('employee')
        .exec(callback);
};
function populateModel(membershipDb, membership) {
    membershipDb.login = membership.login;
    membershipDb.employee.symbol = membership.employee.symbol;
    membershipDb.employee.firstName = membership.employee.firstName
    membershipDb.employee.lastName = membership.employee.lastName;
    membershipDb.employee.position = membership.employee.position;
    membershipDb.employee.kind = membership.employee.kind;
    membershipDb.employee.roles = membership.employee.roles;
}
var update = function (membership, callback) {
    getMembershipById(membership.id, function (err, membershipDb) {
        if (err) {
            callback(err);
        } else {
            populateModel(membershipDb, membership);
            membershipDb.employee.save(function (err) {
                if (err) {
                    callback(err);
                } else {
                    if (membership.password) {
                        hasher.hash(membership.password, function (err, hash) {
                            if (err) {
                                callback(err);
                            } else {
                                membershipDb.password = hash;
                                membershipDb.save(callback);
                            }
                        });
                    } else {
                        membershipDb.save(callback);
                    }
                }
            });
        }
    });
};

exports.update = update;
exports.createNew = createNew;
exports.remove = remove;
exports.getMembership = getMembership;
exports.getMembershipById = getMembershipById;