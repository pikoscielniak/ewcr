var mongoose = require('mongoose'),
    config = require('../core/app/config');

var DbConnectionString = config.dbConnection;
var options = {};
options.server = {};
options.replset = {};
options.socketOptions = options.replset.socketOptions = { keepAlive: 1 };

var connectToDb = function () {
    mongoose.connect(DbConnectionString, options);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function callback() {
        console.log('connection ok');
    });
};

exports.connectToDb = connectToDb;
