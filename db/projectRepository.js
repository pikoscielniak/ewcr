var Project = require('./models').Project;

var create = function (project, callback) {
    Project.create(project, callback);
};

var getById = function (id, callback) {
    Project.findById(id, callback);
};

var getAll = function (callback) {
    Project.find({}, callback);
};

var update = function (project, callback) {
    getById(project._id, function (err, proj) {
        if (err) {
            callback(err);
        }
        proj.number = project.number;
        proj.pwg = project.pwg;
        proj.name = project.name;
        proj.description = project.description,
        proj.startDate = project.startDate;
        proj.endDate = project.endDate;
        proj.status = project.status;
        proj.save(callback);
    });
};

var remove = function (projectId, callback) {
    Project.remove({ _id: projectId }, callback);
};

exports.create = create;
exports.getById = getById;
exports.getAll = getAll;
exports.update = update;
exports.remove = remove;