var Employee = require('./models').Employee;

var getById = function (id, callback) {
    Employee.findById(id, callback);
};

var getAll = function (callback) {
    Employee.find({}, callback);
};

var update = function (employee, callback) {
    getById(employee._id, function (err, emp) {
        if (err) {
            callback(err);
        }
        emp.symbol = employee.symbol;
        emp.firstName = employee.firstName;
        emp.lastName = employee.lastName;
        emp.position = employee.position;
        emp.kind = employee.kind;
        emp.roles = employee.roles;
        emp.save(callback);
    });
};

var getByQuery = function (query, callback) {
    var q = query || '';
    var expQ = new RegExp('.*' + q + '.*', 'i');

    Employee.find({$or: [
        {firstName: expQ},
        {lastName: expQ},
        {symbol: expQ}
    ]}, callback)
};

exports.getById = getById;
exports.getAll = getAll;
exports.getByQuery = getByQuery;
exports.update = update;
