var mongoose = require('mongoose'),
    schemas = require('./schemas.js');

var Employee = mongoose.model('Employee', schemas.employeeSchema);
var Membership = mongoose.model('Membership', schemas.membershipSchema);
var Project = mongoose.model('Project', schemas.projectSchema)
var WorkCard = mongoose.model('WorkCard', schemas.workCardSchema);

exports.Employee = Employee;
exports.Membership = Membership;
exports.Project = Project;
exports.WorkCard = WorkCard;