var WorkCard = require('./models').WorkCard,
    _ = require('underscore');

var create = function (workCard, callback) {
    WorkCard.create(workCard, callback);
};

var remove = function (workCardId, callback) {
    WorkCard.remove({ _id: workCardId }, callback);
};

var getWorkCard = function (employeeId, year, month, callback) {
    WorkCard.findOne({ employee: employeeId, year: year, month: month })
        .populate('employee')
        .populate('workCardItems.project')
        .exec(callback);
};

var workCardExist = function (workCard) {
    return !!workCard;
};

var addWorkCardItem = function (employeeId, year, month, workCardItem, callback) {
    workCardItem.isInProgress = true;
    WorkCard.findOne({ employee: employeeId, year: year, month: month }, function (err, workCard) {
        if (err) {
            callback(err);
        } else {
            if (workCardExist(workCard)) {
                workCard.workCardItems.push(workCardItem);
                workCard.save(callback);
            } else {
                WorkCard.create({
                    month: month,
                    year: year,
                    employee: employeeId,
                    workCardItems: [workCardItem]
                }, callback);
            }
        }
    });
};

var updateWorkCardItem = function (workCardId, workCardItem, callback) {
    WorkCard.findOne({ _id: workCardId}, function (err, workCard) {
        if (err) {
            callback(err);
        } else {
            workCard.workCardItems.id(workCardItem._id).remove();
            workCard.workCardItems.push(workCardItem);
            workCard.save(callback);
        }
    });
};

var removeWorkCardItem = function (workCardId, workCardItemId, callback) {
    WorkCard.findOne({ _id: workCardId}, function (err, workCard) {
        if (err) {
            callback(err);
        } else {
            workCard.workCardItems.id(workCardItemId).remove();
            workCard.save(callback);
        }
    })
};


var getInProgress = function (employeeId, year, month, callback) {
    WorkCard.findOne({ employee: employeeId, year: year, month: month })
        .populate('employee')
        .populate('workCardItems.project')
        .exec(function (err, workCard) {
            if (err) {
                callback(err);
            } else {
                if (workCard) {
                    var itemInProgress = _.find(workCard.workCardItems, function (wci) {
                        return wci.isInProgress == true;
                    });
                    if (itemInProgress) {
                        itemInProgress = itemInProgress.toObject();
                        itemInProgress.workCardId = workCard._id;
                    }
                    callback(null, itemInProgress);
                } else {
                    callback(null, null);
                }
            }
        });
};

exports.create = create;
exports.getWorkCard = getWorkCard;
exports.addWorkCardItem = addWorkCardItem;
exports.updateWorkCardItem = updateWorkCardItem;
exports.removeWorkCardItem = removeWorkCardItem;
exports.remove = remove;
exports.getInProgress = getInProgress;

