var express = require('express'),
    routes = require('./routes'),
    http = require('http'),
    path = require('path'),
    loginer = require('./services/loginer'),
    index = require('./routes/index'),
    appInit = require('./services/appInitiator'),
    employees = require('./routes/employees'),
    projects = require('./routes/projects'),
    workCards = require('./routes/workCards')

var errorHandler = function (err, req, res, next) {
    console.log(err.stack);
    res.json({description: "Błąd"});
};

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('ewcr43242-54243-2342343-2253435'));
app.use(express.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(loginer.authenticate);
app.use(app.router);
app.use(errorHandler);

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', index.index);
app.post('/', index.processLogin);
app.get('/logout', index.processLogout);
app.post('/addEmployee', employees.add);
app.get('/getAllEmployees', employees.getAll);
app.post('/removeEmployee', employees.remove);
app.get('/getEmployee/:id', employees.get);
app.post('/updateEmployee', employees.update);
app.post('/addProject', projects.add);
app.get('/getAllProjects', projects.getAll);
app.get('/getProject/:id', projects.get);
app.post('/updateProject', projects.update);
app.post('/removeProject', projects.remove);
app.get('/getCurrentTask', workCards.getCurrentTask);
app.post('/startTracking', workCards.startTracking);
app.post('/stopTracking', workCards.stopTracking);
app.get('/getWorkCard/:year/:month', workCards.getWorkCard);
app.get('/getWorkCardForEmployee/:employeeId/:year/:month', workCards.getWorkCardForEmployee);
app.post('/updateWorkCard', workCards.updateWorkCard);
app.post('/removeWorkCardItem', workCards.removeWorkCardItem);


http.createServer(app).listen(app.get('port'), function () {
    appInit.initApp();
    console.log('Express server listening on port ' + app.get('port'));
});
