var dbConnection = 'mongodb://ewcr:ewcr123@ds027338.mongolab.com:27338/ewcr';
var systemRoles = {
    employee: 'Employee',
    humanResources: 'Human Resources'
};
var employeeKind = {
    whiteCollarWorker: 'white-collar worker',
    blueCollarWorker: 'blue-collar worker'
};

var settings = {
    dbConnectionString: ''
};

var employeePositions = {
    employee: 'Pracownik',
    humanResourcesManager: 'Pracownik działu kadr'
};

var projectStatuses = {
    opened: 'opened',
    closed: 'closed'
};

var dateFormat = 'YYYY-MM-DD';
var dateTimeFormat = 'YYYY-MM-DD HH:MM:SS';

var systemUser = {
    login: 'admin',
    password: 'admin123',
    employee: {
        symbol: 'admin',
        firstName: 'Admin',
        lastName: 'Admin',
        position: employeePositions.humanResourcesManager,
        kind: employeeKind.whiteCollarWorker,
        roles: [systemRoles.employee, systemRoles.humanResources]
    }
};

exports.systemRoles = systemRoles;
exports.employeeKind = employeeKind;
exports.employeePositions = employeePositions;
exports.projectStatuses = projectStatuses;
exports.systemUser = systemUser;
exports.dbConnection = dbConnection;
exports.dateFormat = dateFormat;
exports.dateTimeFormat = dateTimeFormat;
