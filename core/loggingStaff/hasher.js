var bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var hash = function (string, callback) {

    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) {
            callback(err);
        } else {
            bcrypt.hash(string, salt, function (err, hash) {
                if (err)
                    callback(err);
                else
                    callback(null, hash);
            });
        }
    });
};
var compare = function (candidatePass, password, callback) {
    bcrypt.compare(candidatePass,password,callback);
};
exports.hash = hash;
exports.compare = compare;