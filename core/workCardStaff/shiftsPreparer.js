var _ = require('underscore'),
    shiftCalculator = require('../workCardStaff/shiftCalculator');

var prepareShifts = function (daysInMonth, workCardItems) {
    var shifts = [];
    shifts.length = daysInMonth;
    var i;
    for (i = 1; i <= daysInMonth; i++) {
        var found = _.find(workCardItems, function (item) {
            return item.startTime.getDate() == i;
        });
        if (found) {
            shifts[i - 1] = shiftCalculator.calculateShift(found.startTime);
        }
    }
    return shifts;
};

exports.prepareShifts = prepareShifts;