var _ = require('underscore'),
    totalHourCalculator = require('../workCardStaff/totalHoursCalculator');

var getDistinctProjectIds = function (workCardItems) {
    var projectIds = _.map(workCardItems, function (item) {
        return item.project._id;
    });
    return _.uniq(projectIds);
}

var updateResultWithProjects = function (itemsResult, workCardItems) {
    _.each(itemsResult, function (item) {
        var foundItem = _.find(workCardItems, function (card) {
            return item.projectId == card.project._id;
        });
        item.project = foundItem.project.toObject();
    });
};
var prepareWorkCard = function (daysInMonth, workCardItems) {
    var itemsResult = [];

    var distinctProjectsIds = getDistinctProjectIds(workCardItems);

    if (distinctProjectsIds) {
        var l = distinctProjectsIds.length;
        var k;
        for (k = 0; k < l; k++) {
            var projectResult = {
            };

            var projId = distinctProjectsIds[k];
            projectResult.projectId = projId;
            projectResult.days = [];
            projectResult.days.length = daysInMonth;
            var i;
            for (i = 1; i <= daysInMonth; i++) {
                var itemsOnDay = _.filter(workCardItems, function (item) {
                    return item.startTime.getDate() == i &&
                        item.project._id == projId;
                });
                if (itemsOnDay) {
                    var total = _.reduce(itemsOnDay, function (memo, item) {
                        var hours = 0;
                        if (!item.isInProgress) {
                            hours = totalHourCalculator.calculateTotalHours(item.startTime, item.endTime);
                        }
                        return memo + hours;
                    }, 0);
                    projectResult.days[i - 1] = total;
                }
            }
            itemsResult.push(projectResult);
        }
        updateResultWithProjects(itemsResult, workCardItems)
    }

    return itemsResult;
};

var createReport = function (daysInMonth, workCardItems) {
    var result = [];
    var distinctProjectsIds = getDistinctProjectIds(workCardItems);
    if (distinctProjectsIds) {
        var l = distinctProjectsIds.length;
        var k;
        for (k = 0; k < l; k++) {
            var projectResult = {
            };

            var projId = distinctProjectsIds[k];
            projectResult.projectId = projId;
            var itemsInProject = _.filter(workCardItems, function (item) {
                return item.project._id == projId;
            });

            projectResult.hours = _.reduce(itemsInProject, function (memo, item) {
                var hours = 0;
                if (!item.isInProgress) {
                    hours = totalHourCalculator.calculateTotalHours(item.startTime, item.endTime);
                }
                return memo + hours;
            },0);
            result.push(projectResult);
        }
        updateResultWithProjects(result, workCardItems)
    }

    return result;
};

exports.prepareWorkCard = prepareWorkCard;
exports.createReport = createReport;