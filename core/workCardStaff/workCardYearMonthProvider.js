var dateTime = require('./dateTimeProvider');

var getCurrent = function () {
    var now = dateTime.getNow();
    return {
        year: now.getFullYear(),
        month: now.getMonth()
    }
};

exports.getCurrent = getCurrent;