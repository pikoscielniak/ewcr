var moment = require('moment'),
    config = require('../app/config');

var getNow = function () {
    return new Date();
};
var getDaysForMonth = function (year, month) {
    var date = new Date(year, month, 1, 0, 0, 0, 0);
    return moment(date).daysInMonth();
};

var convertDate = function (date) {
    return moment(date).format(config.dateFormat);
};

var convertDateTime = function (date) {
    return moment(date).format(config.dateTimeFormat);
};


var parseDate = function (dateStr) {
    return moment(dateStr, config.dateFormat).toDate();
};
var parseDateTime = function (dateStr) {
    return moment(dateStr, config.dateTimeFormat).toDate();
};



exports.convertDate = convertDate;
exports.getNow = getNow;
exports.getDaysForMonth = getDaysForMonth;
exports.parseDate = parseDate;
exports.parseDateTime = parseDateTime;
exports.convertDateTime = convertDateTime;