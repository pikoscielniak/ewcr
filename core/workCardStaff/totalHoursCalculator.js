var calculateTotalHours = function (startDate, endDate) {
    if (!!startDate && !!endDate) {
        var difference = endDate.getTime() - startDate.getTime();
        return  Math.floor(difference / (1000 * 3600));
    }
    return 0;
};

exports.calculateTotalHours = calculateTotalHours;