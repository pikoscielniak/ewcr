var firstShiftStart = 6;
var secondShiftStart = 14;
var thirdShiftStart = 22;

var isInFirstShift = function (hour) {
    return hour >= firstShiftStart && hour < secondShiftStart;
};
var isInSecondShift = function (hour) {
    return hour >= secondShiftStart && hour < thirdShiftStart;
};
var calculateShift = function (date) {
    var hour = date.getHours();
    if (isInFirstShift(hour))
        return 1;
    if (isInSecondShift(hour))
        return 2;
    return 3;
};

var getFirstShiftStart = function () {
    return firstShiftStart;
};

var getSecondShiftStart = function () {
    return secondShiftStart;
};

var getThirdShiftStart = function () {
    return thirdShiftStart;
};

exports.calculateShift = calculateShift;
exports.getFirstShiftStart = getFirstShiftStart;
exports.getSecondShiftStart = getSecondShiftStart;
exports.getThirdShiftStart = getThirdShiftStart;