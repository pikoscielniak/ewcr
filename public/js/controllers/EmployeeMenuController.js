ewcrUserApp.controller('EmployeeMenuController', function ($scope, $location) {
    $scope.workCard = function () {
        $location.url('/karty-pracy')
    };
    $scope.projectList = function () {
        $location.url('/lista-projektow')
    };
    $scope.logout = function () {
        window.location = '/logout';
    };
});