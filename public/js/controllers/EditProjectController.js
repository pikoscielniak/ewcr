'use strict';

ewcrHrApp.controller('EditProjectController', function ($scope, $routeParams, $location, projectService) {
    $scope.project = {};

    var getProjectSuccess = function (data) {
        $scope.project = data;
    };
    projectService.get($routeParams.id).
        then(getProjectSuccess);

    $scope.message = {};

    $scope.cancel = function () {
        $location.url('/lista-projektow');
    };

    var saveSuccess = function () {
        $location.url('/lista-projektow');
    };

    var saveFailed = function () {
        $scope.message = 'Wystąpił błąd';
    };

    $scope.save = function (project) {
        projectService.update(project)
            .then(saveSuccess, saveFailed);
    };
});