ewcrUserApp.controller('WorkCardController', function ($scope, workCardService, datesService) {

    function clearWorkCard($scope) {
        $scope.user = null;
        $scope.daysInMonth = 0;
        $scope.shifts = null;
        $scope.workCardItems = null;
    }

    function populateWorkCard($scope, data) {
        $scope.user = data.user;
        $scope.daysInMonth = _.range(1, data.daysInMonth + 1);
        $scope.shifts = data.shifts;
        $scope.workCardItems = data.workCardItems;
    }

    var selectCurrentYearAndMonth = function () {
        $scope.year = datesService.getCurrentYear();
        $scope.month = datesService.getCurrentMonth();
    };

    $scope.months = datesService.months;
    $scope.years = datesService.getYears();

    selectCurrentYearAndMonth();

    $scope.showRaport = function () {
        return !!$scope.user
    };
    $scope.daysInMonth = 0;

    var getWorkCardSuccess = function (data) {
        if (data && data != 'null') {
            populateWorkCard($scope, data);
        } else {
            clearWorkCard($scope);
        }
    };

    $scope.getWorkCard = function (year, month) {
        workCardService.getWorkCard(year, month)
            .then(getWorkCardSuccess);
    };

});