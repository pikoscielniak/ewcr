'use strict';

ewcrUserApp.controller('UserProjectListController', function ($scope, $location, projectService,
                                                              $rootScope, workCardService) {
    $scope.projects = projectService.getAll();

    $scope.startTrackingDisabled = false;

    $scope.$on('inProgress', function (e, value) {
        $scope.startTrackingDisabled = value;
    });

    var startTrackingSuccess = function (data) {
        if (data && data.status) {
            data.data.project = currentProject;
            currentProject = '';
            $rootScope.$broadcast('startedTracking', data.data);
            $scope.startTrackingDisabled = true;
        }
    };

    var currentProject;

    $scope.startTracking = function (project) {
        currentProject = project;
        workCardService.startTracking(project._id)
            .then(startTrackingSuccess);
    };
});