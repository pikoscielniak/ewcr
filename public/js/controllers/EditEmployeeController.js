'use strict';

ewcrHrApp.controller('EditEmployeeController', function ($scope, $routeParams, $location, employeeService) {
    $scope.employee = {};

    var getEmployeeSuccess = function (data) {
        $scope.employee = data;
    };
    employeeService.get($routeParams.id).
        then(getEmployeeSuccess);

    $scope.message = {};

    $scope.cancel = function () {
        $location.url('/lista-pracownikow');
    };

    var saveSuccess = function () {
        $location.url('/lista-pracownikow');
    };

    var saveFailed = function () {
        $scope.message = 'Wystąpił błąd';
    };

    $scope.save = function (employee) {
        employeeService.update(employee)
            .then(saveSuccess, saveFailed);
    };
});