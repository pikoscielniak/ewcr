'use strict';

ewcrHrApp.controller('WorkCardEditController', function ($scope, workCardService, employeeService, datesService) {

    var getAllEmployeesSuccess = function (data) {
        if (data && data != 'null' && data.length > 0) {
            _.each(data, function (item) {
                item.toString = function () {
                    return this.lastName + " " + this.firstName;
                };
            });

            $scope.employees = data;
        }
    };

    employeeService.getAll().then(getAllEmployeesSuccess);

    var getWorkCardSuccess = function (data) {
        if (data && data != 'null') {
            $scope.workCard = data;
        }
    };

    $scope.getWorkCardDisabled = function () {
        return !$scope.e;
    }

    var getWorkCard = function () {
        workCardService.getWorkCardForEmployee($scope.e._id, $scope.year, $scope.month)
            .then(getWorkCardSuccess);
    };

    $scope.getWorkCard = function () {
        getWorkCard();
    };

    $scope.workCard;
    $scope.employees;

    var saveItemSuccess = function (data) {
        if (data && data.status) {
            getWorkCard();
        }
    };

    $scope.saveItem = function (item) {
        workCardService.updateWorkCard($scope.workCard._id, item)
            .then(saveItemSuccess);
    };
    var removeItemSuccess = function (data) {
        if(data && data.status){
            getWorkCard();
        }
    };

    $scope.removeItem = function (item) {
        workCardService.removeWorkCardItem($scope.workCard._id,
            item._id).then(removeItemSuccess);
    };

    var selectCurrentYearAndMonth = function () {
        $scope.year = datesService.getCurrentYear();
        $scope.month = datesService.getCurrentMonth();
    };

    $scope.months = datesService.months;
    $scope.years = datesService.getYears();

    selectCurrentYearAndMonth();
});