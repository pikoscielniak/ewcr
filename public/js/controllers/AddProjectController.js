'use strict';

ewcrHrApp.controller('AddProjectController', function ($scope, $location, projectService) {
    $scope.project = {};

    $scope.message = {};

    $scope.cancel = function () {
        $location.url('/lista-projektow');
    };

    var saveSuccess = function () {
        $location.url('/lista-projektow');
    };

    var saveFailed = function () {
        $scope.message = 'Wystąpił błąd';
    };

    $scope.save = function (project) {
        projectService.add(project)
            .then(saveSuccess, saveFailed);
    };
});