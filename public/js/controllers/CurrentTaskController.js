ewcrUserApp.controller('CurrentTaskController', function ($scope, workCardService, $rootScope, $timeout) {
    var noneTaskMsg = 'Aktualnie nie pracujesz nad żadnym projektem';
    var taskMsg = 'Aktualnie pracujesz nad projektem: ';
    var clockPromise;
    $scope.clock = '';
    $scope.currentTask = null;

    var getCurrentTaskSuccess = function (data) {
        if (!data || data == 'null') {
            $scope.currentTask = null;
        } else {
            $scope.currentTask = data;
            startClock();
        }
        broadcastInProgress();
    };
    var getCurrentTask = function () {
        workCardService.getCurrentTask()
            .then(getCurrentTaskSuccess);
    };

    getCurrentTask();

    $scope.message = function () {
        if ($scope.isInProgress()) {
            return taskMsg + $scope.currentTask.project.name;
        } else {
            return noneTaskMsg;
        }
    };


    var startClock = function () {
        clockPromise = $timeout(function updateClock() {
            $scope.clock = getWorkingTime();
            clockPromise = $timeout(updateClock, 300);
        }, 300);
    };

    var stopClock = function () {
        $timeout.cancel(clockPromise);
    };

    var getWorkingTime = function () {
        var date = (new Date()) - (moment($scope.currentTask.startTime).toDate());
        var result = moment(date).add('hours', -1).format('HH:mm:ss');
        return result;
    };

    $scope.$on('$routeChangeSuccess', function (current) {
        broadcastInProgress();
    });

    $scope.$on('startedTracking', function (e, workItem) {
        $scope.currentTask = workItem;
        startClock();
    });

    var getWorkCardItemData = function () {
        var item = $scope.currentTask;
        return {
            workCardId: item.workCardId,
            startTime: item.startTime,
            _id: item._id,
            projectId: item.project._id
        };
    };
    var stopTrackingSuccess = function (data) {
        if (data.status) {
            stopClock();
            getCurrentTask();
        }
    };

    $scope.stopTracking = function () {
        var data = getWorkCardItemData();
        workCardService.stopTracking(data)
            .then(stopTrackingSuccess);
    };

    $scope.isInProgress = function () {
        return !!$scope.currentTask;
    };

    var broadcastInProgress = function () {
        $rootScope.$broadcast('inProgress', $scope.isInProgress());
    };

    $scope.$on('$destroy', function () {
        $timeout.cancel(clockPromise);
    });
});