'use strict';

ewcrHrApp.controller('AddEmployeeController', function ($scope, $location, employeeService) {
    $scope.employee = {};

    $scope.message = {};

    $scope.cancel = function () {
        $location.url('/lista-pracownikow');
    };

    var saveSuccess = function () {
        $location.url('/lista-pracownikow');
    };

    var saveFailed = function () {
        $scope.message = 'Wystąpił błąd';
    };

    $scope.save = function (employee) {
        employeeService.add(employee)
            .then(saveSuccess, saveFailed);
    };
});