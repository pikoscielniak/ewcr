'use strict';

ewcrHrApp.controller('EmployeeListController', function ($scope, $location, employeeService) {

    $scope.employees = employeeService.getAll();

    $scope.addEmplyee = function () {
        $location.url('/dodaj-pracownika');
    };

    var removeSuccess = function () {
        $scope.employees = employeeService.getAll();
    };

    $scope.edit = function (employee) {
        $location.url('/edycja-pracownika/' + employee._id);
    };

    $scope.remove = function (employee) {
        employeeService.remove(employee._id)
            .then(removeSuccess);
    };
});