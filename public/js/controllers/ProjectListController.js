'use strict';

ewcrHrApp.controller('ProjectListController', function ($scope, $location, projectService) {
    $scope.projects = projectService.getAll();

    $scope.addProject = function () {
        $location.url('/dodaj-projekt');
    };

    var removeSuccess = function () {
        $scope.projects = projectService.getAll();
    };

    $scope.edit = function (project) {
        $location.url('/edycja-projektu/' + project._id);
    };

    $scope.remove = function (project) {
        projectService.remove(project._id)
            .then(removeSuccess);
    };
});