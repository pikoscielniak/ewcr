ewcrHrApp.controller('HrMenuController', function ($scope, $location) {
    $scope.employeeList = function () {
        $location.url('/lista-pracownikow')
    };
    $scope.projectList = function () {
        $location.url('/lista-projektow')
    };
    $scope.workCardsEdit = function () {
        $location.url('/edycja-kart-pracy')
    };

    $scope.logout = function () {
        window.location = '/logout';
    };
});