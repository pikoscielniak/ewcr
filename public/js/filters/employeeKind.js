'use strict';

var employeeKind = function () {
    return function (kind) {
        switch (kind) {
            case 'white-collar worker':
                return "PU";
            case 'blue-collar worker':
                return "PF";
        }
    };
};

ewcrHrApp.filter('employeeKind', employeeKind);
ewcrUserApp.filter('employeeKind', employeeKind);
