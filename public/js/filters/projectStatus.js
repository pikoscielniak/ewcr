'use strict';

var projectStatus = function () {
    return function (status) {
        switch (status) {
            case 'opened':
                return "W toku";
            case 'closed':
                return "Zamknięty";
        }
    };
};

ewcrHrApp.filter('projectStatus', projectStatus);
ewcrUserApp.filter('projectStatus', projectStatus);
