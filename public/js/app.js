'use strict';

var ewcrHrApp = angular.module('ewcrHrApp', []).
    config(function ($routeProvider, $locationProvider) {
        $routeProvider.
            when('/lista-pracownikow', {templateUrl: '/templates/employeeList.html', controller: 'EmployeeListController'}).
            when('/edycja-kart-pracy', {templateUrl: '/templates/workCardEdit.html', controller: 'WorkCardEditController'}).
            when('/dodaj-pracownika', {templateUrl: '/templates/addEmployee.html', controller: 'AddEmployeeController'}).
            when('/edycja-pracownika/:id', {templateUrl: '/templates/editEmployee.html', controller: 'EditEmployeeController'}).
            when('/lista-projektow', {templateUrl: '/templates/projectList.html', controller: 'ProjectListController'}).
            when('/dodaj-projekt', {templateUrl: '/templates/addProject.html', controller: 'AddProjectController'}).
            when('/edycja-projektu/:id', {templateUrl: '/templates/editProject.html', controller: 'EditProjectController'}).
            otherwise({redirectTo: '/lista-pracownikow'});
        $locationProvider.html5Mode(true);
    });

var ewcrUserApp = angular.module('ewcrUserApp', []).
    config(function ($routeProvider, $locationProvider) {
        $routeProvider.
            when('/karty-pracy', {templateUrl: '/templates/workCard.html', controller: 'WorkCardController'}).
            when('/lista-projektow', {templateUrl: '/templates/userProjectList.html',
                controller: 'UserProjectListController'}).
            otherwise({redirectTo: '/karty-pracy'});
        $locationProvider.html5Mode(true);
    });
