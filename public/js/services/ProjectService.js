var projectService = function ($http, $q) {
    var add = function (project) {
        var deferred = $q.defer();
        $http.post('/addProject', project)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var getAll = function () {
        var deferred = $q.defer();
        $http.get('/getAllProjects')
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var remove = function (id) {
        var deferred = $q.defer();
        $http.post('/removeProject', {id: id})
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var get = function (id) {
        var deferred = $q.defer();
        $http.get('/getProject/' + id)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var update = function (project) {
        var deferred = $q.defer();
        $http.post('/updateProject', project)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    return {
        add: add,
        getAll: getAll,
        remove: remove,
        get: get,
        update: update
    }
};

ewcrHrApp.factory('projectService', projectService);
ewcrUserApp.factory('projectService', projectService);