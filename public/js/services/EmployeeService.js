ewcrHrApp.factory('employeeService', function ($http, $q) {
    var add = function (employee) {
        var deferred = $q.defer();
        $http.post('/addEmployee', employee)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var getAll = function () {
        var deferred = $q.defer();
        $http.get('/getAllEmployees')
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var remove = function (id) {
        var deferred = $q.defer();
        $http.post('/removeEmployee', {id: id})
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var get = function (id) {
        var deferred = $q.defer();
        $http.get('/getEmployee/' + id)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var update = function (employee) {
        var deferred = $q.defer();
        $http.post('/updateEmployee', employee)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    return {
        add: add,
        getAll: getAll,
        remove: remove,
        get: get,
        update: update
    }
});