var datesService = function () {

    var getCurrentYear = function () {
        var now = new Date();
        var year = now.getFullYear();
        return year;
    };

    var getCurrentMonth = function () {
        var now = new Date();
        var month = now.getMonth();
        return month;
    };


    var getYears = function () {
        var year = getCurrentYear();

        var result = [];
        for (; year >= 2013; year--) {
            result.push(year);
        }
        return result;
    };

    var months = [
        {name: 'styczeń', value: 0},
        {name: 'luty', value: 1},
        {name: 'marzec', value: 2},
        {name: 'kwiecień', value: 3},
        {name: 'maj', value: 4},
        {name: 'czerwiec', value: 5},
        {name: 'lipiec', value: 6},
        {name: 'sierpień', value: 7},
        {name: 'wrzesień', value: 8},
        {name: 'październik', value: 9},
        {name: 'listopad', value: 10},
        {name: 'grudzień', value: 11}
    ];

    return {
        months: months,
        getCurrentYear: getCurrentYear,
        getCurrentMonth: getCurrentMonth,
        getYears: getYears
    };
};

ewcrHrApp.factory('datesService', datesService);
ewcrUserApp.factory('datesService', datesService);