var workCardService = function ($http, $q) {
    var doPost = function (url, dataToSand) {
        var deferred = $q.defer();
        $http.post(url, dataToSand)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var doGet = function (url) {
        var deferred = $q.defer();
        $http.get(url)
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    };

    var getCurrentTask = function () {
        return doGet('/getCurrentTask');
    };

    var startTracking = function (projectId) {
        return doPost('/startTracking', {projectId: projectId});
    };

    var stopTracking = function (data) {
        return doPost('/stopTracking', data);
    };

    var getWorkCard = function (year, month) {
        var url = '/getWorkCard/' + year + '/' + month;
        return doGet(url);
    };

    var getWorkCardForEmployee = function (employeeId, year, month) {
        var url = '/getWorkCardForEmployee/' + employeeId + '/' + year + '/' + month;
        return doGet(url);
    };

    var updateWorkCard = function (workCardId, workCardItem) {
        var data = {
            workCardId: workCardId,
            workCardItem: workCardItem
        };
        return doPost('/updateWorkCard', data);
    };

    var removeWorkCardItem = function (workCardId, workCardItemId) {
        var data = {
            workCardId: workCardId,
            workCardItemId: workCardItemId
        };
        return doPost('/removeWorkCardItem', data);
    };

    return {
        getCurrentTask: getCurrentTask,
        startTracking: startTracking,
        stopTracking: stopTracking,
        getWorkCard: getWorkCard,
        getWorkCardForEmployee: getWorkCardForEmployee,
        updateWorkCard: updateWorkCard,
        removeWorkCardItem: removeWorkCardItem
    };
};

ewcrHrApp.factory('workCardService', workCardService);
ewcrUserApp.factory('workCardService', workCardService);