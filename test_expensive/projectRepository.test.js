var repository = require('../db/projectRepository'),
    should = require('should'),
    testEnv = require('./testEnvironment'),
    config = require('../core/app/config');

var createdProject;
var ids;

describe('projectRepository', function () {
    describe("create", function () {
        it("should create project", function (done) {
            var proj = testEnv.getNewProject('nr 1');
            repository.create(proj, function (err, project) {
                should.not.exist(err);
                should.exist(project);
                project.number.should.equal(proj.number);
                createdProject = project;
                done();
            });
        });
        it("should create project", function (done) {
            var proj = testEnv.getNewProject('nr 2');
            repository.create(proj, function (err, project) {
                should.not.exist(err);
                should.exist(project);
                project.number.should.equal(proj.number);
                done();
            });
        });
    });
    describe("getById", function () {
        it("should return proper object", function (done) {
            repository.getById(createdProject._id, function (err, project) {
                should.not.exist(err);
                should.exist(project);
                createdProject.number.should.equal(project.number);
                done();
            });
        });
    });
    describe("getAll", function () {
        it("should return all projects", function (done) {
            repository.getAll(function (err, projects) {
                should.not.exist(err);
                should.exist(projects);
                projects.length.should.equal(2);
                ids = projects.map(function (item) {
                    return item._id;
                });
                done();
            });
        });
    });
    describe("update", function () {
        it("should update project", function (done) {
            createdProject.number = 'updated';
            createdProject.pwg = "PWG updated";
            createdProject.name = 'Project test updated';
            createdProject.startDate = new Date(2013, 3, 10);
            createdProject.endDate = new Date(2013, 5, 15);
            createdProject.status = config.projectStatuses.closed;

            repository.update(createdProject, function (err, project) {

                should.not.exist(err);
                should.exist(project);
                createdProject.number.should.equal(project.number);
                createdProject.pwg.should.equal(project.pwg);
                createdProject.name.should.equal(project.name);
                createdProject.startDate.should.equal(project.startDate);
                createdProject.endDate.should.equal(project.endDate);
                createdProject.status.should.equal(project.status);
                done();
            });
        });
    });
    describe("remove", function () {
        it("should remove project", function (done) {
            repository.remove(ids[0], function (err) {
                should.not.exist(err);
                done();
            });
        });
        it("should remove project", function (done) {
            repository.remove(ids[1], function (err) {
                should.not.exist(err);
                done();
            });
        });
    });
});