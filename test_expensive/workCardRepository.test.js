var workCardRepository = require('../db/workCardRepository'),
    membershipRepository = require('../db/membershipRepository.js'),
    projectRepository = require('../db/projectRepository'),
    should = require('should'),
    testEnv = require('./testEnvironment');

var membership1 = testEnv.getMembership('u1', 'pass1', '111');
var membership2 = testEnv.getMembership('u2', 'pass2', '222');
var membership1Id;
var membership2Id;
var createdWorkCard1;
var createdWorkCard2, createdProject;
var createdWorkCardItem;

describe('workCardRepository', function () {
    describe("prepare memberships", function () {
        it("should create memeberships", function (done) {
            membershipRepository.createNew(membership1, function (err, member) {
                should.not.exist(err);
                membership1Id = member._id;
                membershipRepository.createNew(membership2, function (err, member) {
                    should.not.exist(err);
                    membership2Id = member._id;
                    done();
                });
            });
        });
        it("should create project", function (done) {
            projectRepository.create(testEnv.getNewProject('2242'), function (err, project) {
                should.not.exist(err);
                createdProject = project;
                done();
            });
        });
    });
    describe("create", function () {
        it("should create workCard", function (done) {
            var workCard = testEnv.getNewWorkCard(membership1Id);
            workCardRepository.create(workCard, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                workCard.year.should.equal(workCard.year);
                createdWorkCard1 = workCard;
                done();
            });
        });
    });

    describe("getWorkCard", function () {
        it("should return work cart with employee", function (done) {
            workCardRepository.getWorkCard(membership1Id, 2013, 1, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                workCard.year.should.equal(2013);
                workCard.month.should.equal(1);
                should.exist(workCard.employee);
                should.exist(workCard.employee.lastName);
                done();
            });
        });
    });

    describe("addWorkCardItem", function () {
        it("should add work cart item", function (done) {
            var workCardItem = testEnv.getNewWorkCardItem(createdProject._id);
            var year = workCardItem.startTime.getFullYear();
            var month = workCardItem.startTime.getMonth();
            workCardRepository.addWorkCardItem(membership1Id, year, month, workCardItem, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                done();
            });
        });
        it("when work cart does not exist should create work cart and add work cart item", function (done) {
            var workCardItem = testEnv.getNewWorkCardItem(createdProject._id);
            var year = workCardItem.startTime.getFullYear();
            var month = workCardItem.startTime.getMonth();
            workCardRepository.addWorkCardItem(membership2Id, year, month, workCardItem, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                workCard.workCardItems.length.should.equal(1);
                createdWorkCard2 = workCard;
                createdWorkCardItem = workCard.workCardItems[0];
                done();
            });
        });
    });

    describe("getWorkCard", function () {
        it("should return work cart with work cart items populated with project", function (done) {
            workCardRepository.getWorkCard(membership2Id, 2013, 1, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                workCard.workCardItems.length.should.equal(1);
                var project = workCard.workCardItems[0].project;
                should.exist(project);
                done();
            });
        });
    });

    describe("updateWorkCardItem", function () {
        it("should update Work Card item", function (done) {
            createdWorkCardItem.startTime = new Date(2013, 1, 1, 10, 12, 12, 0);
            createdWorkCardItem.endTime = null;
            createdWorkCardItem.isInProgress = true;
            createdWorkCardItem.project = createdProject._id;

            workCardRepository.updateWorkCardItem(createdWorkCard2._id, createdWorkCardItem, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                should.exist(workCard.workCardItems[0]);
                should.not.exist(workCard.workCardItems[0].endTime);
                workCard.workCardItems[0].isInProgress.should.be.true;
                done();
            });
        });
    });

    describe("getInProgress", function () {
        it("should return work card item which is currently in progress", function (done) {
            var year = createdWorkCardItem.startTime.getFullYear();
            var month = createdWorkCardItem.startTime.getMonth();

            workCardRepository.getInProgress(membership2Id, year, month, function (err, workCardItem) {
                should.exist(workCardItem);
                workCardItem.isInProgress.should.be.true;
                done();
            });
        });
    });

    describe("removeWorkCardItem", function () {
        it("should remove Work Card item", function (done) {
            workCardRepository.removeWorkCardItem(createdWorkCard2._id, createdWorkCardItem._id, function (err, workCard) {
                should.not.exist(err);
                should.exist(workCard);
                should.not.exist(workCard.workCardItems[0]);
                done();
            });
        });
    });

    describe("remove", function () {
        it("should remove workCard", function (done) {
            workCardRepository.remove(createdWorkCard1._id, function (err) {
                should.not.exist(err);
                done();
            });
        });
        it("should remove workCard", function (done) {
            workCardRepository.remove(createdWorkCard2._id, function (err) {
                should.not.exist(err);
                done();
            });
        });
    });

    describe('cleanup', function () {
        it('should remove membership and employee', function (done) {
            membershipRepository.remove(membership1Id, function (err) {
                should.not.exist(err);
                done();
            });
        });
        it('should remove membership and employee', function (done) {
            membershipRepository.remove(membership2Id, function (err) {
                should.not.exist(err);
                done();
            });
        });
        it("should remove project", function (done) {
            projectRepository.remove(createdProject._id, function (err) {
                should.not.exist(err);
                done();
            });
        });
    });
});