'use strict';
var memberRepo = require('../db/membershipRepository'),
    employeeRepo = require('../db/employeeRepository'),
    should = require('should'),
    testEnv = require('./testEnvironment'),
    config = require('../core/app/config');

testEnv.connectToDb();
var membership1 = testEnv.getMembership('u1', 'pass1', '111');
var membership2 = testEnv.getMembership('u2', 'pass2', '222');
var membership1Id;
var membership2Id;


describe('membership', function () {
    describe('membershipRepository.createNew', function () {
        it('should add new membership and employee', function (done) {
            memberRepo.createNew(membership1, function (err, member) {
                should.not.exist(err);
                should.exist(member);
                should.exist(member.employee);
                membership1Id = member._id;
                done();
            });
        });
        it('should add new membership and employee with proper symbol', function (done) {
            memberRepo.createNew(membership2, function (err, member) {
                should.not.exist(err);
                should.exist(member);
                should.exist(member.employee);
                membership2Id = member._id;
                done();
            });
        });
    });
    describe('employeeRepository.getById', function () {
        it('should return proper employee', function (done) {
            employeeRepo.getById(membership1Id, function (err, employee) {
                should.not.exist(err);
                should.exist(employee);
                var expectId = membership1Id + '';
                expectId.should.equal(employee._id + '');
                var empSymbol = membership1.employee.symbol;
                empSymbol.should.equal(employee.symbol);
                done();
            });
        });
    });
    describe('employeeRepository.getAll', function () {
        it('should return all employees', function (done) {
            employeeRepo.getAll(function (err, employees) {
                should.not.exist(err);
                should.exist(employees);
                employees.length.should.equal(2);
                done();
            });
        });
    });
    describe('employeeRepository.getByQuery', function () {
        it('when fragment of name is passed, should return proper employee', function (done) {
            var query = 'owal';
            employeeRepo.getByQuery(query, function (err, employees) {
                should.not.exist(err);
                should.exist(employees);
                employees.length.should.equal(2);
                done();
            });
        });
        it('when symbol is passed, should return proper employee', function (done) {
            var query = '111';
            employeeRepo.getByQuery(query, function (err, employees) {
                should.not.exist(err);
                should.exist(employees);
                employees.length.should.equal(1);
                employees[0].symbol.should.equal(query);
                done();
            });
        });
    });
    describe('employeeRepository.update', function () {
        it('should update employee', function (done) {
            var newEmployee = {};
            newEmployee._id = membership1Id;
            newEmployee.symbol = '111';
            newEmployee.firstName = 'A';
            newEmployee.lastName = "B";
            newEmployee.position = config.employeePositions.humanResourcesManager;
            newEmployee.kind = config.employeeKind.whiteCollarWorker;
            newEmployee.roles = [config.systemRoles.humanResources];

            employeeRepo.update(newEmployee, function (err) {
                should.not.exist(err);
                employeeRepo.getById(membership1Id, function (err, employee) {
                    newEmployee.symbol.should.equal(employee.symbol);
                    newEmployee.firstName.should.equal(employee.firstName);
                    newEmployee.lastName.should.equal(employee.lastName);
                    newEmployee.position.should.equal(employee.position);
                    newEmployee.kind.should.equal(employee.kind);
                    newEmployee.roles[0].should.equal(employee.roles[0]);
                    done();
                });
            });
        });
    });
    describe("membershipRepository.getMembership", function () {
        it("should return membership with employee", function (done) {
            memberRepo.getMembership('u1', 'pass1', function (err, membership) {
                should.not.exist(err);
                should.exist(membership);
                should.exist(membership.employee);
                var symbol = membership.employee.symbol;
                symbol.should.be.equal('111');
                done();
            });
        });
        it("should return return null member when memebership doesn't exist", function (done) {
            memberRepo.getMembership('johny', 'pass1', function (err, membership) {
                should.not.exist(err);
                should.not.exist(membership);
                done();
            });
        });
    });
    describe('membershipRepository.remove', function () {
        it('should remove membership and employee', function (done) {
            memberRepo.remove(membership1Id, function (err) {
                should.not.exist(err);
                done();
            });
        });
        it('should remove membership and employee', function (done) {
            memberRepo.remove(membership2Id, function (err) {
                should.not.exist(err);
                done();
            });
        });
    });
});