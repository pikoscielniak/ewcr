var mongoose = require('mongoose'),
    config = require('../core/app/config');

var DbConnectionString = 'mongodb://mongoosetest:mongoosetest@ds033267.mongolab.com:33267/mongoosetest';
var options = {};
options.server = {};
options.replset = {};
options.socketOptions = options.replset.socketOptions = { keepAlive: 1 };

var connectToDb = function () {
    mongoose.connect(DbConnectionString, options);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function callback() {
        console.log('connection ok');
    });
};


var getNewEmployee = function () {
    return {
        symbol: '',
        firstName: 'Jan',
        lastName: 'Kowalski',
        position: config.employeePositions.employee,
        kind: config.employeeKind.blueCollarWorker,
        roles: [config.systemRoles.employee]
    };
};

var getNewMembership = function () {
    return {
        login: '',
        password: '',
        employee: getNewEmployee()
    };
};


var getMembership = function (login, pass, empSymbol) {
    var membership = getNewMembership();
    membership.login = login;
    membership.password = pass;
    membership.employee.symbol = empSymbol;
    return membership;
};

var getNewProject = function (number) {
    return {
        number: number,
        pwg: "PWG",
        name: 'Project test',
        description: 'description',
        startDate: new Date(2013, 3, 5),
        endDate: new Date(2013, 5, 3),
        status: config.projectStatuses.opened
    };
};

var getNewWorkCard = function (employeeId) {
    return {
        month: 1,
        year: 2013,
        employee: employeeId
    };
};

var getNewWorkCardItem = function (projectId) {
    return {
        isInProgress: false,
        startTime: new Date(2013, 1, 1, 12, 12, 12, 0),
        endTime: new Date(2013, 1, 1, 20, 12, 12, 0),
        project: projectId
    };
};

exports.connectToDb = connectToDb;
exports.getMembership = getMembership;
exports.getNewProject = getNewProject;
exports.getNewWorkCard = getNewWorkCard;
exports.getNewWorkCardItem = getNewWorkCardItem;