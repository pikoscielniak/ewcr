var viewDispatcher = require('../services/viewDispatcher');

var authenticate = function (req, res, next) {
    if (req.session.user && req.session.user.isAuthenticated) {
        if (req.xhr) {
            next();
        } else {
            if (req.path === '/logout') {
                next();
            } else {
                viewDispatcher.dispatchView(res, req.session.user);
            }
        }
    } else if (req.path === '/') {
        next();
    } else {
        res.render('index', { title: viewDispatcher.title, errorMsg: '' });
    }
};
exports.authenticate = authenticate;