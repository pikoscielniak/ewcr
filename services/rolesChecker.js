var _ = require('underscore');

var isUserInRole = function (role, roles) {
    var elem = _.find(roles, function (item) {
        return item == role;
    });
    return !!elem;
};

exports.isUserInRole = isUserInRole;