var getDesiredView = function (isHr) {
    if (isHr) {
        return 'indexHr';
    }
    return 'indexEmployee';
};

var getTitle = function (isHr) {
    if (isHr) {
        return 'Panel HR';
    }
    return 'Panel pracownika';
};

var title = 'EWCR - elektroniczna rejestracja kart pracy';

var getUserName = function (user) {
    return user.firstName + ' ' + user.lastName;
};

var dispatchView = function (res, user) {
    var view = getDesiredView(user.isHr);
    var title = getTitle(user.isHr);
    res.render(view, {title: title, userName: getUserName(user)});
};


exports.dispatchView = dispatchView;
exports.title = title;