var membershipRepository = require('../db/membershipRepository'),
    dbInit = require('../db/dbInit'),
    systemUser = require('../core/app/config').systemUser;
var createNewCallback = function (err) {
    if (err) {
        console.log("default user not created");
    }
};

var getMembershipCallback = function (err, member) {
    if (err) {
        console.log("default user not created");
    } else {
        if (!member) {
            membershipRepository.createNew(systemUser, createNewCallback);
        }
    }
};
var checkOrCreateDefaultUser = function () {
    membershipRepository.getMembership(systemUser.login,
        systemUser.password, getMembershipCallback);
};

var initApp = function () {
    dbInit.connectToDb();
    checkOrCreateDefaultUser();
};

exports.initApp = initApp;